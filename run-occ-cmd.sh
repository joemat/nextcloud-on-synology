#!/bin/sh
if [ -z "$1" ];
then
    echo "Usage: $0 <occ command>"
    echo " "
    echo "E.g. $0 db:add-missing-primary-keys"
    exit 1
fi
docker-compose exec -u www-data app php occ $*