# Run nextcloud using docker-compose

## Prereqs

* docker and docker-compose installed

## Steps

* Create the files `.env`, `db.env`, `db-priv.env`, `collabora-priv.env` and `app.env` by copying the templates and adding the missing parameter values.

* run

```
. ./.env
mkdir -p "${MY_DATA_DIR}/nextcloud"
mkdir -p "${MY_DATA_DIR}/mariadb"
mkdir -p "${MY_DATA_DIR}/document
docker-compose up -d
```

* Wait until system is set up (depending on your hardware and internet connectivity the may take very long)
* open https://localhost:443 and follow setup instructions (again: when using slow hardware like synology nas you may run into timeouts - just wait a few minutes and retry)

## Network settings

By default port 443 and 80 are bound to localhost only. This can be changed in the docker-compose.yml.

When your system runs a reverse proxy you can configure it to pass requests to nextcloud. (This is my setup)

## Updating images

See also https://hub.docker.com/_/nextcloud?tab=description

Steps:

```
sudo docker-compose pull
sudo docker-compose up -d
```

(Again be patient, db upgrade may take a while)

## Updating images after changing dockerfiles

```
sudo docker-compose build
sudo docker-compose up -d
```

After update it may be required to run `bash run-occ-cmd.sh upgrade` or some other occ command.


## Cleanup

To stop nextcloud and delete all data run `docker-compose down --rmi local -v`
and delete the data directory afterwards.

# Settings in synology reverse proxy

* nextcloud
    * hostname: nextcloud.<your domain>
    * protocoll: https
    * port: 443
    * target: https, localhost, 6443

* collabora
    * hostname: office.<your domain>
    * protocol: https
    * port: 443
    * target http, localhost, 9980
    * headers: WebSocket

See also https://www.synology-forum.de/threads/nextcloud-mit-collabora.82920/page-6#post-898007
